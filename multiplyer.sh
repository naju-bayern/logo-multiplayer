#!/bin/bash


mkdir logos

cp $1 tmp_in.svg

function farbe {
    cp tmp_in.svg logos/$1.svg
    sed -i "s/#f39315/$2/g" logos/$1.svg
    inkscape -z -e logos/$1.png  --export-dpi 500 logos/$1.svg
}

farbe 'weiss' '#ffffff'
farbe 'schwarz' '#000000'
farbe 'orange' '#f39315'
farbe 'lbv-blau' '#0000FF'

rm tmp_in.svg

zip -r logos.zip logos/
#rm -rf logos/

