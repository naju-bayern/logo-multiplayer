# Logo Multiplyer
Simple script for creating all color versions of a given logo file.

## Usage:
```
./multiplyer.sh logo.svg
```


## Requirements:
* Bash
* Inkscape
